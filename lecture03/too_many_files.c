/* too_many_files.c */

#include <errno.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include <fcntl.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <unistd.h>

int main(int argc, char *argv[]) {
    for (int i = 0; i < atoi(argv[1]); i++) {
        int fd = open("/etc/passwd", O_RDONLY);
        if (fd < 0) {
            fprintf(stderr, "Unable to open: %s\n", strerror(errno));
        }
    }
    return EXIT_SUCCESS;
}

