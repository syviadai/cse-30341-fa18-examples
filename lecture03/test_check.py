#!/usr/bin/env python3

import os
import subprocess
import sys
import tempfile
import unittest

from subprocess import PIPE, STDOUT, DEVNULL

# Basename Test Case

class BasenameTestCase(unittest.TestCase):

    USAGE = 'Usage: ./check flag path\n'                        \
        '   -e path     Returns if path exists\n'               \
        '   -r path     Returns if path is readable\n'          \
        '   -x path     Returns if path is executable\n'        \
        '   -d path     Returns if path is a directory\n'       \
        '   -f path     Returns if path is a regular file\n'    \
        '   -s path     Returns if path exists and has size greater than zero\n'

    @classmethod
    def setUpClass(cls):
        print('\nTesting check...', file=sys.stderr)

    def check_output(self, arguments, expect_failure=False):
        command = './check {}'.format(arguments)
        process = subprocess.run(command, stdout=DEVNULL, stderr=DEVNULL, shell=True)
        if expect_failure:
            self.assertNotEqual(process.returncode, 0)
        else:
            self.assertEqual(process.returncode, 0)

    def check_valgrind(self, arguments):
        command = 'valgrind --leak-check=full ./check {}'.format(arguments)
        process = subprocess.run(command, stdout=PIPE, stderr=STDOUT, shell=True)
        lines   = process.stdout.split(b'\n')
        errors  = [int(l.split()[3]) for l in lines if b'ERROR SUMMARY' in l]

        self.assertEqual(process.returncode, 0)
        self.assertEqual(errors, [0])

    def test_00_help(self):
        command = './check -h'.split()
        process = subprocess.run(command, stdout=PIPE, stderr=STDOUT)
        self.assertEqual(process.stdout.decode(), self.USAGE)

    def test_00_usage(self):
        command = './check'.split()
        process = subprocess.run(command, stdout=PIPE, stderr=STDOUT)
        self.assertEqual(process.stdout.decode(), self.USAGE)

    def test_01_exists(self):
        self.check_output('-e /etc/passwd')

    def test_01_exists_error(self):
        self.check_output('-e /DNE', True)

    def test_01_exists_valgrind(self):
        self.check_valgrind('-e /etc/passwd')

    def test_02_readable(self):
        self.check_output('-r /etc/passwd')

    def test_02_readable_error(self):
        self.check_output('-r /root', True)

    def test_02_readable_valgrind(self):
        self.check_valgrind('-r /etc/passwd')

    def test_03_executable(self):
        self.check_output('-x /bin/ls')

    def test_03_executable_error(self):
        self.check_output('-x /etc/passwd', True)

    def test_03_executable_valgrind(self):
        self.check_valgrind('-x /bin/ls')

    def test_04_directory(self):
        self.check_output('-d /bin')

    def test_04_directory_error(self):
        self.check_output('-d /bin/ls', True)

    def test_04_directory_valgrind(self):
        self.check_valgrind('-d /bin')

    def test_05_directory(self):
        self.check_output('-f /bin/ls')

    def test_05_directory_error(self):
        self.check_output('-f /bin', True)

    def test_05_directory_valgrind(self):
        self.check_valgrind('-f /bin/ls')

    def test_06_directory(self):
        self.check_output('-s /bin/ls')

    def test_06_directory_error(self):
        with tempfile.TemporaryFile() as tf:
            self.check_output('-s {}'.format(tf.name), True)

    def test_06_directory_valgrind(self):
        self.check_valgrind('-s /bin/ls')

# Main Execution

if __name__ == '__main__':
    unittest.main()

# vim: set sts=4 sw=4 ts=8 expandtab ft=python:
