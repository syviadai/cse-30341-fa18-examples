/* slackline.c: Slacklining example (locks, condition variables) */

#include "thread.h"

/* Constants */

const size_t CAPACITY = 3;
const size_t NTHREADS = 1<<10;

/* Global Variables */

size_t NSlackers = 0;
Mutex  Lock      = PTHREAD_MUTEX_INITIALIZER;
Cond   Line      = PTHREAD_COND_INITIALIZER;

/* Functions */

void get_on() {
    mutex_lock(&Lock);
    while (NSlackers >= CAPACITY)
        cond_wait(&Line, &Lock);
    NSlackers++;
    cond_signal(&Line);
    mutex_unlock(&Lock);
}

void get_off() {
    mutex_lock(&Lock);
    NSlackers--;
    cond_signal(&Line);
    mutex_unlock(&Lock);
}

size_t get_nslackers() {
    mutex_lock(&Lock);
    size_t nslackers = NSlackers;
    mutex_unlock(&Lock);
    return nslackers;
}

/* Threads */

void *	slackliner(void *arg) {
    size_t tid = (size_t)arg;
    get_on();
    printf("%lu crosses the slackline : %lu\n", tid, get_nslackers());
    get_off();
    return NULL;
}

/* Main execution */

int main(int argc, char *argv[]) {
    Thread t[NTHREADS];
    for (size_t i = 0; i < NTHREADS; i++)
    	thread_create(&t[i], NULL, slackliner, (void *)i);
    for (size_t i = 0; i < NTHREADS; i++)
    	thread_join(t[i], NULL);
    return 0;
}

/* vim: set expandtab sts=4 sw=4 ts=8 ft=c: */
