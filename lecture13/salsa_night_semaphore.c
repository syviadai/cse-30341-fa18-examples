/* salsa_night_semaphore.c: Salsa night example (semaphore) */

#include "thread.h"
#include <semaphore.h>

/* Constants */

const size_t MIN_FRIENDS = 2;
const size_t NFRIENDS    = 5;

/* Global Variables */

size_t Friends = 0;
sem_t  Lock;
sem_t  Dancing;

/* Threads */

void *	you_dance(void *arg) {
    size_t tid = (size_t)arg;
    sem_wait(&Dancing);
    printf("Thread %lu is dancing!\n", tid);
    return NULL;
}

void *	friend_dance(void *arg) {
    size_t tid = (size_t)arg;
    sem_wait(&Lock);
    Friends++;
    printf("Thread %lu is dancing!\n", tid);
    if (Friends >= MIN_FRIENDS) {
        sem_post(&Dancing);
    }
    sem_post(&Lock);
    return NULL;
}

/* Main execution */

int main(int argc, char *argv[]) {
    Thread t[NFRIENDS + 1];

    sem_init(&Lock, 0, 1);
    sem_init(&Dancing, 0, 0);

    thread_create(&t[0], NULL, you_dance, (void *)0);
    for (size_t i = 1; i <= NFRIENDS; i++) {
    	thread_create(&t[i], NULL, friend_dance, (void *)i);
    }

    for (size_t i = 0; i <= NFRIENDS; i++) {
    	thread_join(t[i], NULL);
    }
    return 0;
}

/* vim: set expandtab sts=4 sw=4 ts=8 ft=c: */
